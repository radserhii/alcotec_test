<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Filters\CatalogProductFilters;

class ProductController extends Controller
{
    /**
     * Get list catalog products by category with filters.
     *
     * @param $categoryId
     * @param CatalogProductFilters $catalogProductFilters
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($categoryId, CatalogProductFilters $catalogProductFilters)
    {
        $productsQuery = Catalog::whereIdCategory($categoryId)->withPriceUAH();

        $totalQuantityOfGoods = $productsQuery->count();

        $productsQuery->filter($catalogProductFilters);

        $totalNumberOfFilteredItems = $productsQuery->count();

        $products = $productsQuery->get();

        return response()->json(compact('products', 'totalNumberOfFilteredItems', 'totalQuantityOfGoods'));
    }
}
