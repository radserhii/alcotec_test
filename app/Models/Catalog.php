<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Filters\{CatalogProductFilters, Filterable};

class Catalog extends Model
{
    use Filterable;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array|bool
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    /**
     * Relation for catalog currency.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(CatalogCurrency::class, 'id_currency');
    }

    /**
     * Scope for count price in UAH depend catalog price and rate.
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithPriceUAH($query)
    {
        return $query->addSelect(
            [
                CatalogProductFilters::SORT_PRICE_MAPPED_NAME => CatalogCurrency::select(
                    DB::raw('catalog_currencies.rate * catalogs.price')
                )
                    ->whereColumn('id', 'catalogs.id_currency')
                    ->limit(1)
            ]
        );
    }
}
