<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatalogCurrency extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array|bool
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];
}
