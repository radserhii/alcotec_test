<?php

namespace App\Filters;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class CatalogProductFilters extends QueryFilters
{
    /**
     * Sort by params names constants.
     */
    const SORT_PRICE_PARAM = 'price';
    const SORT_PRICE_MAPPED_NAME = 'priceUAH';

    /**
     * Request.
     */
    protected $request;

    /**
     * CatalogProductFilters constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    /**
     * Filter by brand.
     *
     * @param $brandId
     * @return Builder
     */
    public function brand($brandId)
    {
        return $this->builder->where('id_brand', $brandId);
    }

    /**
     * Filter by availability.
     *
     * @param $availabilityId
     * @return Builder
     */
    public function availability($availabilityId)
    {
        return $this->builder->where('id_availability', $availabilityId);
    }

    /**
     * Filter by prices range.
     *
     * @param array $pricesRange
     * @return Builder
     */
    public function price(array $pricesRange)
    {
        return $this->builder->leftjoin('catalog_currencies as cc', 'cc.id', '=', 'catalogs.id_currency')
            ->whereBetween(
                DB::raw('cc.rate * catalogs.price'),
                [$pricesRange['from'], $pricesRange['to']]
            );
    }

    /**
     * Sorting handler.
     *
     * @param array $sortData
     * @return Builder
     */
    public function sorting(array $sortData)
    {
        $sortBy = $sortData['sortBy'] === self::SORT_PRICE_PARAM ? self::SORT_PRICE_MAPPED_NAME : $sortData['sortBy'];

        return $this->builder->orderBy($sortBy, $sortData['isDesc'] ? 'desc' : 'asc');
    }
}
