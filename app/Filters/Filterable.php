<?php

namespace App\Filters;

trait Filterable
{
    /**
     * Filters scope for filterable models.
     *
     * @param $query
     * @param QueryFilters $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter($query, QueryFilters $filters)
    {
        return $filters->apply($query);
    }
}
