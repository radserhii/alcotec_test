<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class QueryFilters
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Builder
     */
    protected $builder;

    /**
     * QueryFilters constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply filter method for requests param.
     *
     * @param Builder $builder
     * @return Builder
     */
    public function apply(Builder $builder)
    {
        $this->builder = $builder;

        foreach ($this->filters() as $name => $value) {
            if (!method_exists($this, $name)) {
                continue;
            }
            if ($this->validateRequestValue($value)) {
                $this->$name($value);
            } else {
                $this->$name();
            }
        }

        return $this->builder;
    }

    /**
     * Get requests params for filters.
     *
     * @return array
     */
    public function filters()
    {
        return $this->request->all();
    }

    /**
     * Validate requests value (array or string).
     *
     * @param $value
     * @return bool
     */
    protected function validateRequestValue($value)
    {
        return (is_string($value) && strlen($value)) || (is_array($value) && count($value) || is_numeric($value));
    }
}
