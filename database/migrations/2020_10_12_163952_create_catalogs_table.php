<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedInteger('id_brand');
            $table->unsignedInteger('id_category');
            $table->unsignedTinyInteger('id_availability');
            $table->float('price', 8, 2, true);
            $table->unsignedSmallInteger('id_currency');

            $table->foreign('id_currency')
                ->references('id')
                ->on('catalog_currencies');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogs');
    }
}
