<?php

namespace Database\Seeders;

use App\Models\Catalog;
use App\Models\CatalogCurrency;
use Illuminate\Database\Seeder;

class CatalogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 7) as $count) {
            Catalog::create(
                [
                    'name' => 'Item #' . $count,
                    'id_brand' => $count > 3 ? 80 : mt_rand(1, 79),
                    'id_category' => $count > 3 ? 210 : mt_rand(1, 209),
                    'id_availability' => $count > 3 ? 3 : 2,
                    'price' => mt_rand(0, 400000) / 10,
                    'id_currency' => $count > 3 ? $this->getCurrencyIdByShortName('uah')
                        : $this->getCurrencyIdByShortName('eur'),
                ]
            );
        }
    }

    /**
     * Get currency id by short name.
     *
     * @param $shortName
     * @return mixed
     */
    private function getCurrencyIdByShortName($shortName)
    {
        return CatalogCurrency::whereShortName($shortName)->first('id')->id;
    }
}
