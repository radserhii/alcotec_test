<?php

namespace Database\Seeders;

use App\Models\CatalogCurrency;
use Illuminate\Database\Seeder;

class CatalogCurrenciesTableSeeder extends Seeder
{
    /**
     * Currencies demo data.
     *
     * @var array[]
     */
    private $currencies = [
        [
            'name' => 'Гривна',
            'title' => 'грн',
            'short_name' => 'uah',
            'rate' => 1.00
        ],
        [
            'name' => 'Доллар',
            'title' => '$',
            'short_name' => 'usd',
            'rate' => 28.33
        ],
        [
            'name' => 'Евро',
            'title' => '€',
            'short_name' => 'eur',
            'rate' => 33.36
        ]
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->currencies as $currency) {
            CatalogCurrency::create($currency);
        }
    }
}
